FROM ruby:2

COPY . /var/www/ruby

WORKDIR /var/www/ruby

CMD ["ruby","http_server.rb"]

ENV PORT 8080
EXPOSE 8080

#ENTRYPOINT httpserver

